<?php

use App\Controller\PagesController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->add('home', '/')
        ->controller([PagesController::class, 'home'])
        ->methods(['get'])
    ;
    $routes->add('jobs', '/jobs')
        ->controller([PagesController::class, 'jobs'])
        ->methods(['get'])
    ;
    $routes->add('job', '/jobs/{job}')
        ->controller([PagesController::class, 'job'])
        ->methods(['get'])
    ;
    $routes->add('about', '/about')
        ->controller([PagesController::class, 'about'])
        ->methods(['get'])
    ;
    $routes->add('contact', '/contact')
        ->controller([PagesController::class, 'contact'])
        ->methods(['get'])
    ;
};
