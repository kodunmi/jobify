<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PagesController extends AbstractController
{
    public function home(): Response
    {
        return $this->render('pages/home.html.twig');
    }
    public function jobs(): Response
    {
        return $this->render('pages/jobs.html.twig');
    }
    public function job($job): Response
    {
        return new Response(
           $job
        );
    }
    public function about(): Response
    {
        return $this->render('pages/about.html.twig');

    }
    public function contact(): Response
    {
        return $this->render('pages/contact.html.twig');
    }
}